package com.playsafe.assessment.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class AssessmentService {
    @RequestMapping(path =  "/conversions/ktoc", method = RequestMethod.POST, consumes = "application/json",produces = "application/json")
    public Double convertKtoc(@RequestParam("kelvin") double kelvin) {
        double celcius = 0.0;
        if(kelvin>0){
            celcius = kelvin / 273.15f;
        }
        return celcius;
    }
    @RequestMapping(path =  "/conversions/Ctok", method = RequestMethod.POST, consumes = "application/json",produces = "application/json")
    public Double convertCtok(@RequestParam("celcius") double celcius) {
        double kelvin = 0.0;
        if(celcius>273.15){
            kelvin = 273.15 + celcius;
        }
        return kelvin;
    }
    @RequestMapping(path =  "/conversions/Mtok", method = RequestMethod.POST, consumes = "application/json",produces = "application/json")
    public Double convertMtok(@RequestParam("miles") double miles) {
        log.info("starting");
        double kilometers = 0.0;
        if(miles>0){
            kilometers = 1.6 * miles;
        }
        return kilometers;
    }
    @RequestMapping(path =  "/conversions/Ktom", method = RequestMethod.POST, consumes = "application/json",produces = "application/json")
    public Double convertKtom(@RequestParam("kilometers") double kilometers) {
        double miles = 0.0;
        if(kilometers>0){
            miles = 0.621 * kilometers;
        }
        return miles;
    }

    }

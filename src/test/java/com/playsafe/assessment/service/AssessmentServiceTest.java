package com.playsafe.assessment.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.Test;


class AssessmentServiceTest {

    @Test
    void convertKtoc() {
        double k = 10.0;
        double celcius = 0.0;
        if(k>0){
            celcius = 10 - 273.15;
            Assert.assertEquals(283.15,283.15,0.000);
        }
    }

    @Test
    void convertCtok() {
            double c = 10.0;
            double kelvin = 0.0;
            if(c>0){
                kelvin = 273.15f + c;
                Assert.assertEquals(283.15,283.15,0.000);
            }
    }

    @Test
    void convertMtok() {
        double m = 10.0;
        double kilometers = 0.0;
        if(m>0){
            kilometers = 1.6 * m;
            Assert.assertEquals(16,16,0.000);
        }
    }

    @Test
    void convertMtom() {
        double km = 10.0;
        double miles = 0.621 * -1;//;10.0;
        if(km>0){
            miles = 0.621 * km;
            Assert.assertEquals(6.21,6.21,0.000);
        }

    }
}